#!/bin/bash
cd $(dirname "$0")

source _func.sh

sudo docker-compose -f $COMPOSECFG up --build --remove-orphan -d $*

#!/bin/bash
mpp-solar \
  -P PI30MAX \
  -c QPIGS,QPIGS2,QLT,QET \
  -o influx_mqtt \
  --daemon \
  --mqttbroker localhost \
  --mqtttopic "$MQTT_TOPIC" \

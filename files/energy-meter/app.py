import asyncio, socket
from modbus_client.pymodbus_async_modbus_client import PyAsyncModbusRtuClient
from modbus_device.modbus_device import ModbusDevice


async def main():
    modbus_client = PyAsyncModbusRtuClient(path="/dev/ttyUSB0", baudrate=9600, stopbits=1, parity='N', timeout=3)
    modbus_device = ModbusDevice.create_from_file("DDS238")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def send(name, value):
        msg = f"HV1,setting={name} value={value:.3f}"
        # print(msg)
        sock.sendto(msg.encode("ascii"), ("192.168.1.20", 8094))

    while True:
        voltage = await modbus_device.read_register(modbus_client, unit=1, register="voltage")
        energy = await modbus_device.read_register(modbus_client, unit=1, register="energy")
        current = await modbus_device.read_register(modbus_client, unit=1, register="current")
        active_power = await modbus_device.read_register(modbus_client, unit=1, register="active_power")
        reactive_power = await modbus_device.read_register(modbus_client, unit=1, register="reactive_power")
        power_factor = await modbus_device.read_register(modbus_client, unit=1, register="power_factor")
        frequency = await modbus_device.read_register(modbus_client, unit=1, register="frequency")

        send("voltage", voltage)
        send("energy", energy)
        send("current", current)
        send("active_power", active_power)
        send("reactive_power", reactive_power)
        send("power_factor", power_factor)
        send("frequency", frequency)

        await asyncio.sleep(5)


asyncio.get_event_loop().run_until_complete(main())

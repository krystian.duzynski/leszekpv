import binascii
import socket
import struct
import time
import traceback
from typing import Dict

import serial

serial = serial.Serial(port="/dev/ttyUSB0", timeout=1,
                       baudrate=115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
                       stopbits=serial.STOPBITS_ONE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send(values: Dict[str, float]):
    msg = f"JKBMS " + ",".join(f"{key}={value:.3f}" for key, value in values.items())
    # print(msg)
    sock.sendto(msg.encode("ascii"), ("192.168.1.20", 8094))


class BinaryReader:
    def __init__(self, buffer):
        self.buffer = buffer
        self.pos = 0

    def read_bytes(self, n):
        b = self.buffer[self.pos:self.pos + n]
        self.pos += n
        return b


def read():
    serial.flushInput()
    serial.flushOutput()
    serial.write(binascii.unhexlify(
        "4e 57 00 13 00 00 00 00 06 03 00 00 00 00 00 00 68 00 00 01 29".replace(" ", "")))

    resp = serial.read(1000)

    br = BinaryReader(resp)

    br.read_bytes(2)
    length = struct.unpack(">H", br.read_bytes(2))[0]
    assert length == 283
    br.read_bytes(4)
    br.read_bytes(1)
    br.read_bytes(1)
    br.read_bytes(1)
    N = length - 18
    payload = br.read_bytes(N)
    payload = BinaryReader(payload)

    assert payload.read_bytes(1) == b"\x79"
    assert payload.read_bytes(1) == b"\x2a"

    data = {}
    for i in range(14):
        cell_num = payload.read_bytes(1)
        cell_mv = struct.unpack(">H", payload.read_bytes(2))[0]
        data[f"cell_{i + 1}"] = cell_mv

    assert payload.read_bytes(1) == b"\x80"
    data["power_tube_temp"] = struct.unpack(">H", payload.read_bytes(2))[0]
    assert payload.read_bytes(1) == b"\x81"
    data["balance_plate_temp"] = struct.unpack(">H", payload.read_bytes(2))[0]
    assert payload.read_bytes(1) == b"\x82"
    data["battery_temp"] = struct.unpack(">H", payload.read_bytes(2))[0]
    assert payload.read_bytes(1) == b"\x83"
    data["voltage"] = struct.unpack(">H", payload.read_bytes(2))[0] * 10
    assert payload.read_bytes(1) == b"\x84"
    bdata = payload.read_bytes(2)
    current_raw = struct.unpack(">H", bdata)[0]
    if current_raw > 32768:
        current_raw = current_raw - 32768
    else:
        current_raw = -current_raw
    data["current"] = current_raw / 100
    # print(data)

    return data


def main():
    while True:
        data = read()
        send(data)

        time.sleep(2)


main()

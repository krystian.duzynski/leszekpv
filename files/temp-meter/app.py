import asyncio, socket
import sys

from modbus_client.pymodbus_async_modbus_client import PyAsyncModbusRtuClient
from modbus_client.exceptions import ReadErrorException
from modbus_device.modbus_device import ModbusDevice


async def main():
    modbus_client = PyAsyncModbusRtuClient(path="/dev/ttyUSB0", baudrate=9600, stopbits=1, parity='N', timeout=3)
    modbus_device = ModbusDevice.create_from_file("XY-MD02")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def send(name, value, unit):
        msg = f"TEMP,setting={name},unit={unit} value={value:.3f}"
        # print(msg)
        sock.sendto(msg.encode("ascii"), ("192.168.1.20", 8094))

    os_errors = 0

    while True:
        for sid in [1, 2, 3]:
            try:
                temp = await modbus_device.read_register(modbus_client, unit=sid, register="temp")
                humidity = await modbus_device.read_register(modbus_client, unit=sid, register="humidity")
                send("temp", temp, sid)
                send("humidity", humidity, sid)
                os_errors = 0
            except ReadErrorException:
                print(f"READ ERROR, unit={sid}")
                os_errors = 0
            except OSError:
                print(f"OS ERROR, unit={sid}")
                os_errors += 1

                if os_errors > 5:
                    sys.exit(1)

        await asyncio.sleep(3)


asyncio.get_event_loop().run_until_complete(main())

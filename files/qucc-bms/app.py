import socket
import struct
import time
import traceback
from typing import Dict

import serial

serial = serial.Serial(port="/dev/ttyUSB0", timeout=1,
                       baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


class ReadException(Exception):
    pass


def send(values: Dict[str, float]):
    msg = f"QUCCBMS " + ",".join(f"{key}={value:.3f}" for key, value in values.items())
    # print(msg)
    sock.sendto(msg.encode("ascii"), ("192.168.1.20", 8094))


def read_response(expected_cmd):
    start_mark = serial.read(1)

    if len(start_mark) != 1 or start_mark[0] != 0xdd:
        raise ReadException()

    resp_code = serial.read(1)

    if len(resp_code) != 1 or resp_code[0] != expected_cmd:
        raise ReadException()

    status_len = serial.read(2)
    if len(status_len) != 2:
        raise ReadException()

    status, len_ = struct.unpack("BB", status_len)
    if status != 0:
        raise ReadException()

    data_payload = serial.read(len_)

    chk_end = serial.read(3)
    if len(chk_end) != 3:
        raise ReadException()

    s = sum(struct.pack("BB", status, len_) + data_payload)
    crc_data = struct.pack(">H", 0xffff - s + 1)
    assert chk_end[:-1] == crc_data

    return data_payload


def send_frame_for_response(cmd, payload):
    data_w_len = struct.pack("BB", cmd, len(payload)) + payload

    s = sum(data_w_len)
    crc_data = struct.pack(">H", 0xffff - s + 1)

    READ = 0xa5
    cmdb = struct.pack("BB", 0xdd, READ) + data_w_len + crc_data + b"\x77"

    serial.flushInput()
    serial.flushOutput()
    serial.write(cmdb)

    return read_response(cmd)


def send_command_for_response(cmd):
    return send_frame_for_response(cmd, b"")


def read_3():
    data_payload = send_command_for_response(3)

    fmt = ">HhHH"
    raw_voltage, raw_current, raw_remaining_capacity, *_ = struct.unpack(fmt, data_payload[:struct.calcsize(fmt)])

    voltage = raw_voltage * 10
    current = raw_current * 10
    remaining_capacity = raw_remaining_capacity * 10

    return {
        "voltage": voltage,
        "current": current,
        "remaining_capacity": remaining_capacity,
    }


def read_4():
    data_payload = send_command_for_response(4)
    count = len(data_payload) // 2

    data = {}
    for i in range(count):
        p = data_payload[i * 2:i * 2 + 2]
        mv = struct.unpack(">H", p)
        data[f"cell_{i + 1}"] = mv[0]
    return data


def main():
    while True:
        try:
            data = read_3()
            send(data)
        except ReadException:
            pass
        except:
            traceback.print_exc()

        try:
            data = read_4()
            send(data)
        except ReadException:
            pass
        except:
            traceback.print_exc()

        time.sleep(2)


main()
